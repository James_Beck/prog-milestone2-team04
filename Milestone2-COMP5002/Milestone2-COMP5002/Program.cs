﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Milestone2_COMP5002
{
    class Program
    {
        static void Main(string[] args)
        {
            Task2method1();
        }

        //Task2method1 is the summary of information for Task 2 of the project
        //Task2method1 stores the variables that are used amongst the other methods
        static void Task2method1()
        {
            //Storage of variables for Task2method1
            string input;
            int output = 0;
            int papers;
            int studentid = 0;
            int i = 0;
            string grade;
            string suffix;
            List<string> storepapers = new List<string>();
            List<int> storegrades = new List<int>();           

            //Pause in the code until a whole number is input
            do
            {
                Console.WriteLine("Hi user, what's you're student id?");
                input = Console.ReadLine();
                int.TryParse(input, out studentid);
                Console.Clear();
            } while (studentid < 1);


            //label to refer to to correct grade information
            level:

            //do loop to get the grade of someone with only to valid inputs to continue: 5 and 6
            do
            {
                Console.WriteLine("Sweet sauce, and what level are you studying: 5 or 6? (students studying level 5 study 4 papers, students studying level 6 study 3 papers)");
                grade = Console.ReadLine();
                int.TryParse(grade, out output);
                Console.Clear();
            } while (output < 5 || output > 6);

            //Changing the value of papers for it's frequent future use
            if (output == 5)
            {
                papers = 4;
            }
            else
            {
                papers = 3;
            }

            //Reiterate the information for the user so they can check if they've made a mistake
            Console.WriteLine("Awesome, thanks for that. Considering that you're level {0} you must have done {1} papers last semester.", output, papers);

            //Creates a suffix to correctly ask the user for the information
            do
            {
                if (i == 0)
                {
                    suffix = "st";
                }
                else if (i == 1)
                {
                    suffix = "nd";
                }
                else if (i == 2)
                {
                    suffix = "rd";
                }
                else
                {
                    suffix = "th";
                }

                //label to refer back to so that the same paper name isn't entered twice
            repeat:

                //Ask the user to enter the papers they did and the corresponding grade of that paper
                Console.WriteLine("Great! Prepare to enter the papers you study (make sure that you don't add a double up)?");
                Console.WriteLine("Enter the {0}{1} paper you did.", i + 1, suffix);
                input = Console.ReadLine();
                input.Replace(" ", string.Empty);
                input = input.ToUpper();
                
                //Checks whether the paper input has been entered before and if so, repeats the prompt asking for the paper
                if (storepapers.Contains(input))
                {
                    Console.WriteLine("No, you've already entered that value, enter another one.");
                    Thread.Sleep(1000);
                    Console.Clear();
                    goto repeat;
                }
                storepapers.Add(input);

                //Forces a number value for the grade to be input
                do
                {
                    output = -1;
                    Console.WriteLine("Out of 100, what grade did you get in {0}?", storepapers[i]);
                    input = Console.ReadLine();
                    try
                    {
                        output = int.Parse(input);
                    }
                    catch
                    { }
                } while (output < 0 || output > 100);
                storegrades.Add(output);
                Console.Clear();
                i++;
            } while (i < papers);

            //Prompts for a summary of information, which is stored in another method
            Console.WriteLine("Would you like to see a summary of you're information (yes or no)?");
            input = Console.ReadLine().ToLower();
            if (input == "yes")
            {
                Console.WriteLine();
                Task2method2(storepapers, storegrades, studentid, papers, i, grade);
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }
            else { }

            //Prompts for a grade point average, which is stored in another method
            Console.WriteLine();
            Console.WriteLine("Would you like to see your grade point average (yes or no)?");
            input = Console.ReadLine().ToLower();
            if (input == "yes")
            {
                Task2method3(storegrades, i, papers);
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }

            //Prompts for the appearance of which papers the student has gotten an A+ in, stored in another method
            Console.WriteLine();
            Console.WriteLine("Would you like to see in which papers you got an A+?");
            input = Console.ReadLine().ToLower();
            if (input == "yes")
            {
                Task2method4(storepapers, storegrades, i, papers);
                Console.ReadKey();
            }
        }

        //Task2method2 deals with the conversion of the numerical grade values into letter values
        //Task2method2 holds the display for the grades (in letter form), student id information, and the studying level
        static void Task2method2(List<string> storepapers, List<int> storegrades, int studentid, int papers, int i, string grade)
        {
            for (i = 0; i < papers; i++)
            {
                Console.Write(storepapers[i]);
                Console.Write(" --- ");
                if (storegrades[i] > 89)
                {
                    Console.WriteLine("A+");
                }
                else if (storegrades[i] > 84 && storegrades[i] < 90)
                {
                    Console.WriteLine("A");
                }
                else if (storegrades[i] > 79 && storegrades[i] < 85)
                {
                    Console.WriteLine("A-");
                }
                else if (storegrades[i] > 74 && storegrades[i] < 80)
                {
                    Console.WriteLine("B+");
                }
                else if (storegrades[i] > 69 && storegrades[i] < 75)
                {
                    Console.WriteLine("B");
                }
                else if (storegrades[i] > 64 && storegrades[i] < 70)
                {
                    Console.WriteLine("B-");
                }
                else if (storegrades[i] > 59 && storegrades[i] < 65)
                {
                    Console.WriteLine("C+");
                }
                else if (storegrades[i] > 54 && storegrades[i] < 60)
                {
                    Console.WriteLine("C");
                }
                else if (storegrades[i] > 49 && storegrades[i] < 55)
                {
                    Console.WriteLine("C+");
                }
                else if (storegrades[i] > 39 && storegrades[i] < 50)
                {
                    Console.WriteLine("D");
                }
                else if (storegrades[i] < 40)
                {
                    Console.WriteLine("E");
                }
            }
            Console.WriteLine();
            Console.WriteLine("You're student id is {0}", studentid);
            Console.WriteLine();
            Console.WriteLine("You are studying level {0}", grade);
            Console.WriteLine();
        }

        //Task2method3 shows the value for the grade point average and is the method which displays that information
        static void Task2method3(List<int> storegrades, int i, int papers)
        {
            int gpa = 0;

            for (i = 0; i < papers; i++)
            {
                gpa = gpa + storegrades[i];
            }
            Console.WriteLine();
            if (papers == 4)
            {
                Console.WriteLine("You're grades were {0}, {1}, {2}, {3}, and your grade point average is {4}", storegrades[0], storegrades[1], storegrades[2], storegrades[3], gpa / papers);
            }
            else if (papers == 3)
            {
                Console.WriteLine("You're grades were {0}, {1}, {2}, and your grade point average is {3}", storegrades[0], storegrades[1], storegrades[2], gpa / papers);
            }

            //Friendly response to the user regarding whether they have succeeded or not
            if (gpa / papers > 50)
            {
                Console.WriteLine("Hey you passed!");
            }
            else
            {
                Console.WriteLine("Unfortunately, you didn't pass the course.");
            }
        }

        //Task2method4 shows the quantity of A+ grades (grades that were numerical 90+) and which papers those grades were in
        static void Task2method4 (List<string> storepapers, List<int> storegrades, int i, int papers)
        {
            for (i = 0; i < papers; i++)
            {
                if (storegrades[i] > 89)
                {
                    Console.WriteLine("In the {0} course you managed to score an A+", storepapers[i]);
                }
            }
        }
    }
}
